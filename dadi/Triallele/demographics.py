"""
Equilibrium, two epoch, and three epoch (bottleneck) model with selection (sig1, sig2) on the two derived alleles
"""

from . import numerics, integration
import numpy as np
import dadi
from numpy import newaxis as nuax

from dadi.Triallele.TriSpectrum_mod import TriSpectrum

def equilibrium(params, ns, pts, sig1 = 0.0, sig2 = 0.0, theta1 = 1.0, theta2 = 1.0, misid = 0.0, dt = 0.005, folded = False):
    """
    Description:
        Integrate the density function to equilibrium

        params is unused

    Arguments:
        sig1 float: Population scaled selection coefficient for the first dervied allele
        sig2 float: Population scaled selection coefficient for the second derived allele
        theta1 float: Population scaled mutation rate for the first derived allele
        theta2 float: Population scaled mutation rate for the second derived allele
        misid float: Ancestral misidentification parameter
        dt float: Time step to use for integration
        folded boolean: If True, fold the frequency spectrum (if we assume we don't know the order that derived alleles appeared)
    """
    x = np.linspace(0,1,pts+1)
    sig1,sig2 = np.float(sig1),np.float(sig2)

    y1 = dadi.PhiManip.phi_1D(x,gamma=sig1)
    y2 = dadi.PhiManip.phi_1D(x,gamma=sig2)
    phi = np.zeros((len(x),len(x)))
    if sig1 == sig2 == 0.0 and theta1 == theta2 == 1:
        phi = integration.equilibrium_neutral_exact(x)
    else:
        phi = integration.equilibrium_neutral_exact(x)
        phi,y1,y2 = integration.advance(phi, x, 2, y1, y2, nu=1., sig1=sig1, sig2=sig2, theta1=theta1, theta2=theta2, dt=dt)

    dx = numerics.grid_dx(x)

    try:
        ns = int(ns)
    except TypeError:
        ns = ns[0]

    fs = numerics.sample(phi, ns, x)
    fs.extrap_t = dt

    if folded == True:
        fs = fs.fold_major()

    if misid > 0.0:
        fs = numerics.misidentification(fs,misid)
    
    return fs
    
def two_epoch(params, ns, pts, sig1 = 0.0, sig2 = 0.0, theta1 = 1.0, theta2 = 1.0, misid = 0.0, dt = 0.005, folded = False):
    """
    Description:
        A single population size change at some point in the past

        params = (nu, T)

    Arguments:
        nu float: Relative poplulation size change to ancestral population size
        T float: Time in past that size change occured (scaled by 2Na generations)
        sig1 float: Population scaled selection coefficient for the first derived allele
        sig2 float: Population scaled selection coefficient for the two derived allele
        theta1 float: Population scaled mutation rate for the first derived allele 
        theta2 float: Population scaled mutation rate for the second derived allele
        misid float: Ancestral misidentification parameter
        dt float: Time step to use for integration
        folded boolean: If True, fold the frequency spectrum
    """
    nu,T = params

    x = np.linspace(0,1,pts+1)
    sig1,sig2 = np.float(sig1),np.float(sig2)

    y1 = dadi.PhiManip.phi_1D(x,gamma=sig1)
    y2 = dadi.PhiManip.phi_1D(x,gamma=sig2)
    phi = np.zeros((len(x),len(x)))

    # integrate to equilibrium first
    if sig1 == sig2 == 0.0 and theta1 == theta2 == 1:
        phi = integration.equilibrium_neutral_exact(x)
    else:
        phi = integration.equilibrium_neutral_exact(x)
        phi,y1,y2 = integration.advance(phi, x, 2, y1, y2, nu=1., sig1=sig1, sig2=sig2, theta1=theta1, theta2=theta2, dt=dt)

    phi,y1,y2 = integration.advance(phi, x, T, y1, y2, nu=nu, sig1=sig1, sig2=sig2, theta1=theta1, theta2=theta2, dt=dt)

    dx = numerics.grid_dx(x)

    try:
        ns = int(ns)
    except TypeError:
        ns = ns[0]

    fs = numerics.sample(phi, ns, x)
    fs.extrap_t = dt

    if folded == True:
        fs = fs.fold_major()
    
    if misid > 0.0:
        fs = numerics.misidentification(fs,misid)
    
    return fs


def three_epoch(params, ns, pts, sig1 = 0.0, sig2 = 0.0, theta1 = 1.0, theta2 = 1.0, misid = 0.0, dt = 0.005, folded = False):
    """
    Description:
        Two instantaneous population size changes in the past

        params = (nu1, nu2, T1, T2)

    Arguments:
        nu1 float: Relative population size change to ancestral population size in the second epoch
        nu2 float: Relative poplulation size changes to ancestral population size in the third epoch (nu1 occurs before nu2, historically)
        T1 float: Time for which population had relative size nu1 (scaled by 2Na generations)
        T2 float: Time for which population had relative size nu2 (scaled by 2Na generations)
        sig1 float: Population scaled selection coefficient for the first derived allele
        sig2 float: population scaled selection coefficient for the second derived allele
        theta1 float: Population scaled mutation rate for the first derived allele
        theta2 float: Population scaled mutation rate for the second derived allele
        misid float: Ancestral misidentification parameter
        dt float: Time step to use for integration
        folded boolean: If True, fold the frequency spectrum
    """
    nu1,nu2,T1,T2 = params
    x = np.linspace(0,1,pts+1)
    sig1,sig2 = np.float(sig1),np.float(sig2)
    
    y1 = dadi.PhiManip.phi_1D(x,gamma=sig1)
    y2 = dadi.PhiManip.phi_1D(x,gamma=sig2)
    phi = np.zeros((len(x),len(x)))
    
    # integrate to equilibrium first
    if sig1 == sig2 == 0.0 and theta1 == theta2 == 1:
        phi = integration.equilibrium_neutral_exact(x)
    else:
        phi = integration.equilibrium_neutral_exact(x)
        phi,y1,y2 = integration.advance(phi, x, 2, y1, y2, nu=1., sig1=sig1, sig2=sig2, theta1=theta1, theta2=theta2, dt=dt)
    
    phi,y1,y2 = integration.advance(phi, x, T1, y1, y2, nu1, sig1, sig2, theta1, theta2, dt=dt)
    phi,y1,y2 = integration.advance(phi, x, T2, y1, y2, nu2, sig1, sig2, theta1, theta2, dt=dt)

    dx = numerics.grid_dx(x)
    
    try:
        ns = int(ns)
    except TypeError:
        ns = ns[0]
    
    fs = numerics.sample(phi, ns, x)
    fs.extrap_t = dt

    if folded == True:
        fs = fs.fold_major()
    
    if misid > 0.0:
        fs = numerics.misidentification(fs,misid)
    
    return fs

def bottlegrowth(params, ns, pts, sig1 = 0.0, sig2 = 0.0, theta1 = 1.0, theta2 = 1.0, misid = 0.0, dt = 0.005, folded = False):
    """
    Description:
        Instantanous size change followed by exponential growth.

        params = (nuB, nuF, T)

    Arguments:
        nuB float: Ratio of population size after instantanous change to ancient population size
        nuF float: Ratio of contemporary to ancient population size
        T float: Time in the past at which instantaneous change happened and growth began (in units of 2Na generations)
        sig1 float: Population scaled selection coefficient for the first derived allele 
        sig2 float: Population scaled selection coefficient for the second derived allele
        theta1 float: Population scaled mutation rate for the first derived allele
        theta2 float: Population scaled mutation rate for the second derived allele
        misid float: Ancestral misidentification parameter
        dt float: Time step to use for integration
        folded boolean: If True, fold the frequency spectrum
    """
    nuB,nuF,T = params
    if nuB == nuF:
        nu = nuB
    else:
        nu = lambda t: nuB*np.exp(np.log(nuF/nuB) * t/T)
    
    x = np.linspace(0,1,pts+1)
    sig1,sig2 = np.float(sig1),np.float(sig2)
    
    y1 = dadi.PhiManip.phi_1D(x,gamma=sig1)
    y2 = dadi.PhiManip.phi_1D(x,gamma=sig2)
    phi = np.zeros((len(x),len(x)))
    
    # integrate to equilibrium first
    if sig1 == sig2 == 0.0 and theta1 == theta2 == 1:
        phi = integration.equilibrium_neutral_exact(x)
    else:
        phi = integration.equilibrium_neutral_exact(x)
        phi,y1,y2 = integration.advance(phi, x, 2, y1, y2, nu=1., sig1=sig1, sig2=sig2, theta1=theta1, theta2=theta2, dt=dt)
    
    phi,y1,y2 = integration.advance(phi, x, T, y1, y2, nu, sig1, sig2, theta1, theta2, dt=dt)

    dx = numerics.grid_dx(x)
    
    try:
        ns = int(ns)
    except TypeError:
        ns = ns[0]
    
    fs = numerics.sample(phi, ns, x)
    fs.extrap_t = dt

    if folded == True:
        fs = fs.fold_major()
    
    if misid > 0.0:
        fs = numerics.misidentification(fs,misid)
    
    return fs
