"""
Two population demographic models.
"""
import numpy

from dadi import Numerics, PhiManip, Integration
from dadi.Spectrum_mod import Spectrum

def snm(notused, ns, pts):
    """
    Description:
        ns = (n1, n2)

        Standard neutral model, populations never diverge.

    Arguments:
        n1 int: Sample size of the first population in the resulting Spectrum.
        n2 int: Sample size of the second population in the resulting Spectrum.

    Returns:
        dadi.Spectrum_mod: The resulting frequency spectrum.
    """
    xx = Numerics.default_grid(pts)
    phi = PhiManip.phi_1D(xx)
    phi = PhiManip.phi_1D_to_2D(xx, phi)
    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs

def bottlegrowth(params, ns, pts):
    """
    Description:
        Instantanous size change followed by exponential growth with no population split.
        
        params = (nuB, nuF, T)

        ns = (n1, n2)

    Arguments:
        nuB float: Ratio of population size after instantanous change to ancient population size.
        nuF float: Ratio of contempoary to ancient population size.
        T float: Time in the past at which instantaneous change happened and growth began (in units of 2*Na generations).
        n1 int: Sample size of the first population in the resulting Spectrum.
        n2 int: Sample size of the second population in the resulting Spectrum.
        pts int: Number of grid points to use in integration.

    Returns:
        dadi.Spectrum_mod: The resulting frequency spectrum.
    """
    nuB,nuF,T = params
    return bottlegrowth_split_mig((nuB,nuF,0,T,0), ns, pts)

def bottlegrowth_split(params, ns, pts):
    """
    Description:
        Instantanous size change followed by exponential growth then split.
    
        params = (nuB, nuF, T, Ts)

        ns = (n1, n2)

    Arguments:
        nuB float: Ratio of population size after instantanous change to ancient population size.
        nuF float: Ratio of contempoary to ancient population size.
        T float: Time in the past at which instantaneous change happened and growth began (in units of 2*Na generations).
        Ts float: Time in the past at which the two populations split.
        n1 int: Sample size of the first population in the resulting Spectrum.
        n2 int: Sample size of the second population in the resulting Spectrum.
        pts int: Number of grid points to use in integration.

    Returns:
        dadi.Spectrum_mod: The resulting frequency spectrum.
    """
    nuB,nuF,T,Ts = params
    return bottlegrowth_split_mig((nuB,nuF,0,T,Ts), ns, pts)

def bottlegrowth_split_mig(params, ns, pts):
    """
    Description:
        Instantanous size change followed by exponential growth then split with migration.

        params = (nuB, nuF, m, T, Ts)

        ns = (n1, n2)

    Arguments:
        nuB float: Ratio of population size after instantanous change to ancient population size.
        nuF float: Ratio of contempoary to ancient population size.
        m float: Migration rate between the two populations (2*Na*m).
        T float: Time in the past at which instantaneous change happened and growth began (in units of 2*Na generations).
        Ts float: Time in the past at which the two populations split.
        n1 int: Sample size of the first population in the resulting Spectrum.
        n2 int: Sample size of the second population in the resulting Spectrum.
        pts int: Number of grid points to use in integration.

    Returns:
        dadi.Spectrum_mod: The resulting frequency spectrum.
    """
    nuB,nuF,m,T,Ts = params

    xx = Numerics.default_grid(pts)
    phi = PhiManip.phi_1D(xx)

    if T >= Ts:
        nu_func = lambda t: nuB*numpy.exp(numpy.log(nuF/nuB) * t/T)
        phi = Integration.one_pop(phi, xx, T-Ts, nu_func)

        phi = PhiManip.phi_1D_to_2D(xx, phi)
        nu0 = nu_func(T-Ts)
        nu_func = lambda t: nu0*numpy.exp(numpy.log(nuF/nu0) * t/Ts)
        phi = Integration.two_pops(phi, xx, Ts, nu_func, nu_func, m12=m, m21=m)
    else:
        phi = PhiManip.phi_1D_to_2D(xx, phi)
        phi = Integration.two_pops(phi, xx, Ts-T, 1, 1, m12=m, m21=m)
        nu_func = lambda t: nuB*numpy.exp(numpy.log(nuF/nuB) * t/T)
        phi = Integration.two_pops(phi, xx, T, nu_func, nu_func, m12=m, m21=m)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs

def split_mig(params, ns, pts):
    """
    Description:
        Split into two populations of specifed size, with migration.
        
        params = (nu1, nu2, T, m)

        ns = (n1, n2)


    Arguments:
        nu1 float: Size of population 1 after split.
        nu2 float: Size of population 2 after split.
        T float: Time in the past of split (in units of 2*Na generations).
        m float: Migration rate between populations (2*Na*m).
        n1 int: Sample size of the first population in the resulting Spectrum.
        n2 int: Sample size of the second population in the resulting Spectrum.
        pts int: Number of grid points to use in integration.

    Returns:
        dadi.Spectrum_mod: The resulting frequency spectrum.
    """
    nu1,nu2,T,m = params

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx)
    phi = PhiManip.phi_1D_to_2D(xx, phi)

    phi = Integration.two_pops(phi, xx, T, nu1, nu2, m12=m, m21=m)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs

def split_mig_mscore(params):
    """
    Description:
        ms core command for split_mig.
    """
    nu1,nu2,T,m = params

    command = "-n 1 %(nu1)f -n 2 %(nu2)f "\
            "-ma x %(m12)f %(m21)f x "\
            "-ej %(T)f 2 1 -en %(T)f 1 1"

    sub_dict = {'nu1':nu1, 'nu2':nu2, 'm12':2*m, 'm21':2*m, 'T': T/2}

    return command % sub_dict

def IM(params, ns, pts):
    """
    Description:
        Isolation-with-migration model with exponential pop growth.

        ns = (n1, n2)

        params = (s, nu1, nu2, T, m12, m21)

    Arguments:
        s float: Size of pop 1 after split. (Pop 2 has size 1-s.).
        nu1 float: Final size of pop 1.
        nu2 float: Final size of pop 2.
        T float: Time in the past of split (in units of 2*Na generations).
        m12 float: Migration from pop 2 to pop 1 (2*Na*m12).
        m21 float: Migration from pop 1 to pop 2.
        n1 int: Sample size of the first population in the resulting Spectrum.
        n2 int: Sample size of the second population in the resulting Spectrum.
        pts int: Number of grid points to use in integration.

    Returns:
        dadi.Spectrum_mod: The resulting frequency spectrum.
    """
    s,nu1,nu2,T,m12,m21 = params

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx)
    phi = PhiManip.phi_1D_to_2D(xx, phi)

    nu1_func = lambda t: s * (nu1/s)**(t/T)
    nu2_func = lambda t: (1-s) * (nu2/(1-s))**(t/T)
    phi = Integration.two_pops(phi, xx, T, nu1_func, nu2_func,
                               m12=m12, m21=m21)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs

def IM_mscore(params):
    """
    Description:
        ms core command for IM.
    """
    s,nu1,nu2,T,m12,m21 = params

    alpha1 = numpy.log(nu1/s)/T
    alpha2 = numpy.log(nu2/(1-s))/T
    command = "-n 1 %(nu1)f -n 2 %(nu2)f "\
            "-eg 0 1 %(alpha1)f -eg 0 2 %(alpha2)f "\
            "-ma x %(m12)f %(m21)f x "\
            "-ej %(T)f 2 1 -en %(T)f 1 1"

    sub_dict = {'nu1':nu1, 'nu2':nu2, 'alpha1':2*alpha1, 'alpha2':2*alpha2,
                'm12':2*m12, 'm21':2*m21, 'T': T/2}

    return command % sub_dict

def IM_pre(params, ns, pts):
    """
    Description:
        Isolation-with-migration model with exponential pop growth and a size change prior to split.
    
        params = (nuPre, TPre, s, nu1, nu2, T, m12, m21)

        ns = (n1, n2)

    Arguments:
        nuPre float: Size after first size change.
        TPre float: Time before split of first size change.
        s float: Fraction of nuPre that goes to pop1. (Pop 2 has size nuPre*(1-s).)
        nu1 float: Final size of pop 1.
        nu2 float: Final size of pop 2.
        T float: Time in the past of split (in units of 2*Na generations).
        m12 float: Migration from pop 2 to pop 1 (2*Na*m12).
        m21 float: Migration from pop 1 to pop 2.
        n1 int: Sample size of the first population in the resulting Spectrum.
        n2 int: Sample size of the second population in the resulting Spectrum.
        pts int: Number of grid points to use in integration.

    Returns:
        dadi.Spectrum_mod: The resulting frequency spectrum.
    """
    nuPre,TPre,s,nu1,nu2,T,m12,m21 = params

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx)
    phi = Integration.one_pop(phi, xx, TPre, nu=nuPre)
    phi = PhiManip.phi_1D_to_2D(xx, phi)

    nu1_0 = nuPre*s
    nu2_0 = nuPre*(1-s)
    nu1_func = lambda t: nu1_0 * (nu1/nu1_0)**(t/T)
    nu2_func = lambda t: nu2_0 * (nu2/nu2_0)**(t/T)
    phi = Integration.two_pops(phi, xx, T, nu1_func, nu2_func,
                               m12=m12, m21=m21)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs

def IM_pre_mscore(params):
    """
    Description:
        ms core command for IM_pre.
    """
    nuPre,TPre,s,nu1,nu2,T,m12,m21 = params
    
    nu1_0 = nuPre*s
    nu2_0 = nuPre*(1-s)
    alpha1 = numpy.log(nu1/nu1_0)/T
    alpha2 = numpy.log(nu2/nu2_0)/T
    command = "-n 1 %(nu1)f -n 2 %(nu2)f "\
            "-eg 0 1 %(alpha1)f -eg 0 2 %(alpha2)f "\
            "-ma x %(m12)f %(m21)f x "\
            "-ej %(T)f 2 1 -en %(T)f 1 %(nuP)f "\
            "-en %(TP)f 1 1"

    sub_dict = {'nu1':nu1, 'nu2':nu2, 'alpha1':2*alpha1, 'alpha2':2*alpha2,
                'm12':2*m12, 'm21':2*m21, 'T': T/2, 'nuP':nuPre, 'TP':(T+TPre)/2}

    return command % sub_dict

def IM_fsc(params, ns, pts):
    """
    Description:
        Split into two populations of specifed size, with asymetric migration.
        
        params = (nu1, nu2, T, m12, m21)

        ns = (n1, n2)

    Arguments:
        nu1 float: Size of population 1 after split.
        nu2 float: Size of population 2 after split.
        T float: Time in the past of split (in units of 2*Na generations).
        m12 float: Migration from pop 2 to pop 1 (2*Na*m12).
        m21 float: Migration from pop 1 to pop 2 (2*Na*m21).
        n1 int: Sample size of the first population in the resulting Spectrum.
        n2 int: Sample size of the second population in the resulting Spectrum.
        pts int: Number of grid points to use in integration.

    Returns:
        dadi.Spectrum_mod: The resulting frequency spectrum.
    """
    nu1,nu2,T,m12,m21 = params

    xx = Numerics.default_grid(pts)

    phi = PhiManip.phi_1D(xx)
    phi = PhiManip.phi_1D_to_2D(xx, phi)

    phi = Integration.two_pops(phi, xx, T, nu1, nu2, m12=m12, m21=m21)

    fs = Spectrum.from_phi(phi, ns, (xx,xx))
    return fs
