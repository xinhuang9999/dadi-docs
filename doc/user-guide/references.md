# References

1. Hernandez RD, Williamson SH, Bustamante CD (2007) Context dependence, ancestral misidentification, and spurious signatures of natural selection. *Mol Biol Evol* **24**: 1792–1800. DOI: [10.1093/molbev/msm108](https://doi.org/10.1093/molbev/msm108)
2. Weir BS, Cockerham CC (1984) Estimating F-statistics for the analysis of population structure. *Evolution* **38**: 1358–1370. DOI: [10.1111/j.1558-5646.1984.tb05657.x](https://doi.org/10.1111/j.1558-5646.1984.tb05657.x)
3. Gutenkunst RN, Hernandez RD, Williams SH, Bustamante CD (2009) Inferring the joint demographic history of multiple populations from multidimensional SNP frequency data. *PLoS Genet* **5**: e1000695. DOI: [10.1371/journal.pgen.1000695](https://doi.org/10.1371/journal.pgen.1000695)
4. Pierce DA, Schafer DW (1986) Residuals in generalized linear models. *J Am Stat Assoc* **81**: 977–986. DOI: [10.1080/01621459.1986.10478361](https://doi.org/10.1080/01621459.1986.10478361)
5. Ku HH (1966) Notes on the use of propagation of error formulas. *J Res Nbs C Eng Inst* **70**: 263–273. [Link](https://nvlpubs.nist.gov/nistpubs/jres/70C/jresv70Cn4p263_A1b.pdf)
6. Coffman AJ, Hsieh P, Gravel S, Gutenkunst RN (2016) Computationally efficient composite likelihood statistics for demographic inference. *Mol Biol Evol* **33**: 591–593. DOI: [10.1093/molbev/msv255](https://doi.org/10.1093/molbev/msv255)
7. Self SG, Liang KY (1987) Asymptotic properties of maximum likelihood estimates and likelihood ratio tests under nonstandard conditions. *J Am Stat Assoc* **82**: 605–610. DOI: [10.2307/2289471](https://doi.org/10.2307/2289471)
8. Kim BY, Huber CD, Lohmueller KE (2017) Inference of the distribution of selection coefficients for new nonsynonymous mutations using large samples. *Genetics* **206**: 345. DOI: [10.1534/genetics.116.197145](https://doi.org/10.1534/genetics.116.197145)
9. Ortega-Del Vecchyo D, Marsden CD, Lohmueller KE (2016) Prefersim: Fast simulation of demography and selection under the poisson random field model. *Bioinformatics* **32**: 3516–3518. DOI: [10.1093/bioinformatics/btw478](https://doi.org/10.1093/bioinformatics/btw478)
10. Haller BC, Messer PW (2017) Slim 2: Flexible, interactive forward genetic simulations. *Mol Biol Evol* **34**: 230–240. DOI: [10.1093/molbev/msw211](https://doi.org/10.1093/molbev/msw211)
